from locust import HttpUser, TaskSet, task, between
import json


class UserTasks(TaskSet):
    
    @task(1)
    def login(self):
        payload = {
            "email": "developers@paystand.com",
            "password": "Paystand!23"
        }
        headers = {'content-type': 'application/json'}
        self.client.post(
            "/api/v3/users/login",
            data=payload,
            headers=headers
        )
       

class WebsiteUser(HttpUser):
    """
    User class that does requests to the locust web server running on localhost
    """

    host = "https://localhost:3001"
    wait_time = between(2, 5)
    tasks = [UserTasks]
